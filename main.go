package main

import (
	"github.com/labstack/echo/v4"
)

func main() {
	e := echo.New()
	e.GET("/search", HandleSearch)
	e.GET("/api/qt/stock/kline/get", HandleApi)
	e.GET("/api/qt/otcfund/kline/get", HandleApi)
	e.Logger.Fatal(e.Start(":9000"))
}