package main

import (
	"io"
	"net/http"

	"github.com/labstack/echo/v4"
)


const SearchUrl = "https://search-codetable.eastmoney.com/codetable/search/web/wap"

const ApiUrl = "https://push2his.eastmoney.com"

func HandleSearch(c echo.Context) error  {
	params := c.Request().URL.RawQuery

	resp, err := http.Get(SearchUrl + "?" + params)

	if err != nil {
		return err
	}

	defer resp.Body.Close()

	_, err = io.Copy(c.Response().Writer, resp.Body)

	return err
}

func HandleApi(c echo.Context) error {
	params := c.Request().URL.RawQuery
	path := c.Request().URL.RawPath
	u := ApiUrl + path + "?" + params

	resp, err := http.Get(u)

	if err != nil {
		return err
	}

	defer resp.Body.Close()

	_, err = io.Copy(c.Response().Writer, resp.Body)

	return err

}
